(ns app.routing
  (:require
   [clojure.spec.alpha :as s]
   [taoensso.timbre :as timbre]
   [reitit.core :as router]
   [reitit.frontend :as frontend]
   [reitit.frontend.easy :as easy]
   [reitit.coercion :as coercion]
   [reitit.coercion.spec :as rspec]
   [re-frame.core :as re-frame]
   [mount.core :refer [defstate]]))

(s/def ::name string?)

(s/def ::request (s/keys :opt-un [::name]))

(def routes
    [["/" {:name :app/home
           :parameters {:query ::request}}]
     ["/signin" {:name :app/signin}]
     ["/exit" {:name :app/exit}]
     ["/enter" {:name :app/enter}]])

(re-frame/reg-fx
 ::navigate!
 (fn [k params query]
   (easy/push-state k params query)))

(re-frame/reg-event-fx
 ::navigate
 (fn [{:keys [db] :as fx} [_ match]]
   (timbre/debug "Navigate:" match)
   (let [name (get-in match [:data :name])
         params (get-in match [:path-params])
         query (get-in match [:query-params])]
     {:dispatch [name {:params params :query query}]})))

(defn init! []
  (easy/start!
    (frontend/router routes {:data {:coercion rspec/coercion}})
    (fn [new-match]
      (timbre/debug "Route match:" new-match)
      (re-frame/dispatch [::navigate new-match]))
    ;; set to false to enable HistoryAPI
    ;; if true query args aren't passed on...
    ;; should perhaps be reported as issue to reitit?
    {:use-fragment false}))

#_
(defstate routing-state
  :start (init!))
