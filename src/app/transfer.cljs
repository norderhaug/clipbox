(ns app.transfer
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop]])
  (:require
   [cljs.core.async :as async
    :refer [<! chan close! alts! timeout put!]]
   [cljs-http.client :as http]
   [taoensso.timbre :as timbre]
   [re-frame.core :as rf]
   [mount.core :refer [defstate]]
   [app.lib.clipboard :as clipboard
    :refer [modern-clipboard]]
   [app.lib.datatransfer :as datatransfer
    :refer [as-url]]
   [app.model.invoice :as invoice]
   [app.model.clip
    :refer [->Clip]]
   [app.lightning :as lightning]
   [cljs-drag-n-drop.core :as dnd]))

"Content transfer through clipboard (copy/cut/paste) and filesystem drag/drop"

; https://google.github.io/closure-library/api/goog.events.FileDropHandler.html
; https://github.com/google/closure-library/blob/master/closure/goog/demos/dragdrop.html
; https://github.com/google/closure-library/blob/master/closure/goog/demos/drag.html
; https://google.github.io/closure-library/api/goog.fx.DragDrop.html
; https://github.com/tonsky/cljs-drag-n-drop

(defn- encode-clip-item [{:keys [type data] :as item}]
  (cond
    (and (= type "text/plain")
         (string? data)
         (lightning/valid-invoice? data))
    (let [encoded data
          decoded (lightning/decode-invoice encoded)]
      (invoice/create encoded decoded))
    :else
    item))

(defn dispatch-paste [payload]
  {:pre [(sequential? payload)]}
  (timbre/debug "Dispatch Paste:" (count payload) payload)
  (->> payload
   (map #(assoc % :id (random-uuid)))
   (map encode-clip-item)
   (->Clip)
   (vector :paste)
   (rf/dispatch)))

(defn dispatch-copy [clipboard-insert]
  (let [; board-items @(rf/subscribe [:board])
        selection @(rf/subscribe [:selected])]
     (timbre/info "Copy to clipboard:" selection)
     (when selection
       (clipboard-insert selection))))

(defn dispatch-clip-paste [payload]
  ;; ## TODO: Eliminate by payload not being js object [HACK]
  (timbre/debug "Clip Paste:" payload)
  (cond
    (sequential? payload)
    (dispatch-paste payload)
    :else
    (timbre/warn "Ignored paste payload:" payload)))

(defn enable-clipboard []
  (clipboard/subscribe!
   {:copy dispatch-copy
    :paste dispatch-clip-paste}))

(defn disable-clipboard []
  (clipboard/unsubscribe!))

(defstate clipboard-target
  :start (enable-clipboard)
  :stop (disable-clipboard))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn drop-handler [& transfer-items]
  {:pre [(every? sequential? transfer-items)]}
  (timbre/debug "Drop Handler:" (count transfer-items) (map count transfer-items)
                transfer-items)
  (doseq [group transfer-items]
    (dispatch-paste group)))

(defn enable-drop []
  (dnd/subscribe! js/document.documentElement :unique-key
                  {:start (fn [e]
                            (println "d1 start")
                            (rf/dispatch [:drag [:start e]]))
                   :enter (fn [e]
                            (println "d1 enter")
                            (rf/dispatch [:drag [:enter e]]))
                   :drop  (->
                           (fn [transfer-items]
                             (drop-handler transfer-items)
                             (rf/dispatch [:drag [:drop transfer-items]]))
                           (datatransfer/dnd-drop-wrapper))
                   :leave (fn [e]
                            (println "d1 leave")
                            (rf/dispatch [:drag [:leave e]]))
                   :end   (fn [e]
                            (println "d1 end")
                            (rf/dispatch [:drag [:end e]]))}))


(defn disable-drop []
  (dnd/unsubscribe! js/document.documentElement :unique-key))

(defstate drop-target
  :start (enable-drop)
  :stop (disable-drop))
