(ns app.view.board
  (:require
    [taoensso.timbre :as timbre]
    ["@material-ui/core" :as mui]
    ["@material-ui/core/Grid" :default Grid]
    ["@material-ui/icons/FileCopy" :default CopyIcon]
    ;[app.lib.reagent-mui
    ; :refer [error-boundary]]
    [app.view.clip
     :refer [clip-card]]
    [app.lib.styling :as styling]
    [re-frame.core :as rf]))

(def debug (rf/subscribe [:debug]))

(defn board-listing [{:keys [items]}]
  (into
   [:> mui/List]
   (for [{:keys [selected] :as item} items]
     [:> mui/ListItem
      {:selected (boolean selected)}
      [clip-card item]])))

(def board-grid-styles (styling/mui-styles
                        (fn [theme]
                          {:grid-bg {:padding (.spacing theme 1)
                                     :margin (.spacing theme 4)}
                           :board-grid-item {:max-height "calc(50vh  - 46px)"}})))

(defn board-grid [{:keys [items]}]
  [styling/block {:mui-styles board-grid-styles
                  :class-name ["grid-bg"]}
   (fn [{:keys [board-grid-item] :as classes}]
      (into
          [:> Grid {:container true :spacing 6}]
          (for [{:keys [id selected] :as item} items]
            ^{:key id}
           [:> Grid {:item true :xs 12 :sm 6 :md 4
                     :classes {:root board-grid-item}}
             #_{:selected (boolean selected)}
               [clip-card item]])))])

(def drag-status (rf/subscribe [:drag]))

(defn drop-zone [& children]
  ;; consider instead using https://www.npmjs.com/package/material-ui-dropzone
  (into
   [:div
    {:style {:min-height "100vh"
             :width "100%"
             :border (if (= @drag-status :start)
                       "thick solid yellow"
                       "thin solid none")}}]
   children))

(defn board-pane [items]
  [drop-zone
   [:> mui/Hidden {:sm-up true}
    [board-listing {:items items}]]
   [:> mui/Hidden {:xs-down true}
    [board-grid {:items items}]]])
