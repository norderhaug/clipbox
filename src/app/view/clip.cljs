(ns app.view.clip
  (:require
    [clojure.spec.alpha :as s]
    [taoensso.timbre :as timbre]
    [reagent.core :as reagent]
    ["@material-ui/core" :as mui]
    ["@material-ui/icons/FileCopy" :default CopyIcon]
    ["@material-ui/icons/FlashOn" :default LightningIcon]
    ["@material-ui/icons/InsertDriveFile" :default FileIcon]
    ["@material-ui/icons/ExpandMore" :default ExpandIcon]
    ["@material-ui/core/Tabs" :default Tabs]
    ["@material-ui/core/Tab" :default Tab]
    ["@material-ui/core/ExpansionPanel" :default ExpansionPanel]
    ["@material-ui/core/ExpansionPanelDetails" :default ExpansionPanelDetails]
    ["@material-ui/core/ExpansionPanelSummary" :default ExpansionPanelSummary]
    [app.model.invoice
     :refer [Invoice]]
    [app.model.clip
     :refer [Clip]]
    [re-frame.core :as rf]))

(def debug (rf/subscribe [:debug]))

(defn lightning-icon []
  [:> LightningIcon
    {:style {:color "yellow"}}])

(defn avatar-element [icon]
  (-> [:> mui/Avatar icon]
      reagent/as-element))

(defn card-file-area [{:keys [kind name id type data url] :as item}]
  [:<>
   [:> mui/CardHeader
     {:title name
      :subheader type
      :avatar (avatar-element [:> FileIcon])}]])

(defn labeled-row [label & content]
  (into [:> mui/TableRow {:key (str label)}
         [:> mui/TableCell (str label)]]
        content))

(defn card-image-area [{:keys [kind id type data url]
                           :as item}]
  [:> mui/CardActionArea
   (if @debug
     [:div (str url)])
   [:> mui/CardMedia
    {; :title url
     :style {:min-height "140px"}
     :component "img"
     :image url}]

   (if @debug
     [:> mui/CardContent
      [:> mui/Typography {:component "p"}
       "Image: " type]
      [:> mui/Typography {:component "p"}
       url]])])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti content-label type)

(defmethod content-label :default [{:keys [kind id type data url] :as item}]
  {:post [string?]}
  type)

(defmethod content-label Invoice [item]
  "Lightning Invoice")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti clip-content type)

(defmethod clip-content :default [{:keys [kind id type data url]
                                   :as item}]
   (timbre/debug "Render variation:" item)
   (cond
     (s/valid? :app.model.clip/mime-text-plain type)
     [:> mui/CardContent
      [:> mui/Typography {:component "p"} data]]

     (s/valid? :app.model.clip/mime-text-html type)
     [:> mui/CardContent
       [:> mui/Typography {:component "code"} data]]

     (and url (s/valid? :app.model.clip/mime-image type))
     [card-image-area item]

     (= kind "file") ;; some image clips show up as file
     [card-file-area item]

     @debug
     [:> mui/CardContent
      (pr-str data)]))

(def opennode-checkout-url "https://checkout.opennode.co/420a1a3e-5096-4c58-89d3-71771406be30")

(defmethod clip-content Invoice [{:keys [encoded decoded]
                                  :as item}]
  [:<>
   [:> mui/CardHeader
    {:title "Lightning Invoice"
     :subheader encoded}]
   [:> mui/CardContent
    [:> mui/Table
     [:> mui/TableBody
      [labeled-row "Amount"
       [:> mui/TableCell {}(str (:satoshis decoded) " SAT")]]
      [labeled-row "Coin Type"
       [:> mui/TableCell {}(str ( :coinType decoded))]]
      (if-let [[memo] (filter #(= (:tagName %) "description")
                           (:tags decoded))]
       [labeled-row "Memo"
        [:> mui/TableCell {}(str (:data memo))]])]]]
   [:> mui/CardActions
    [:> mui/Button
     {:on-click #(set! js/window.location
                   opennode-checkout-url)}
     "Pay Now"]]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti clip-card type)

(defmethod clip-card :default [{:keys [kind id type data url]
                                :as item}]
  (timbre/debug "Render clip-card:" item)
  [:> mui/Card
   {:on-click #(rf/dispatch [:select item :toggle])}
   [clip-content item]])

(defn clip-card-v0 [{:keys [variations] :as clip}]
  ; Minimalistic
  [:> mui/Card
   {:on-click #(rf/dispatch [:select clip :toggle])}
   (for [{:keys [type] :as v} variations]
      (case type
        "text/html" nil
       ^{:key type}
       [clip-content v]))])

#_
(defn clip-card-v1 [{:keys [variations] :as clip}]
  ; simple
  [:> mui/Card
   {:on-click #(rf/dispatch [:select clip :toggle])}
   (for [{:keys [type] :as v} variations]
     ^{:key type}
      [clip-content v])])

#_
(defn clip-card-v2 [clip]
  ; a version with a tab, perhaps reserve this for an 'advanced' mode?
  (let [variation (reagent/atom nil)]
    (fn [{:keys [variations] :as clip}]
      (timbre/debug "Render Clip:" clip)
      [:> mui/Card
       {:on-click #(rf/dispatch [:select clip :toggle])}
       (if (rest variations)
         (into [:> mui/Tabs
                {:value (:type (or @variation (first variations)))
                 :on-change (fn [e value]
                              (->> (-> (group-by :type variations)
                                       (get-in [value 0]))
                                   (reset! variation)))}
                (for [{:keys [type] :as v} variations]
                   ^{:key type}
                   [:> mui/Tab {:label (content-label v)
                                :value type}])]))
       (if @variation
         [clip-content @variation]
         (for [variation variations]
           ^{:key (:type variation)}
            [clip-content variation]))])))

#_
(defn clip-card-v3 [{:keys [variations] :as clip}]
  ; ## FIX: not yet showing all of an invoice, missing table
  [:> mui/Card
   {:on-click #(rf/dispatch [:select clip :toggle])}
   (for [{:keys [type] :as v} variations]
     ^{:key type}
      [:<>
       [:> mui/ExpansionPanel
         [:> mui/ExpansionPanelSummary
           {:expand-icon [:> ExpandIcon]}
           [:> mui/CardHeader {:title (content-label v)}]]
         [:> mui/ExpansionPanelDetails
           [clip-content v]]]])])

(defmethod clip-card Clip [clip]
  (clip-card-v0 clip))
