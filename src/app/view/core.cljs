(ns app.view.core
  (:require
   [taoensso.timbre :as timbre]
   ["@material-ui/core" :as mui]
   ["@material-ui/core/styles/MuiThemeProvider" :default mui-ThemeProvider]
   ["@material-ui/core/styles" :refer [createMuiTheme]]
   ["@material-ui/core/colors" :as mui-colors]
   [re-frame.core :as rf]
   [reagent.core :as reagent]
   [app.state :refer [app-state]]
   [app.events]
   [app.view.dev :as dev]
   [app.view.appbar :as appbar
    :refer [appbar]]
   [app.view.board
    :refer [board-pane]]
   [mount.core :refer [defstate]]))

;; https://v3-8-0.material-ui.com/

(def user-data (rf/subscribe [:blockstack/user-data]))

(defn custom-theme []
  (timbre/info "Use custom theme")
  (createMuiTheme
   (clj->js {:palette {:type "light"
                       :primary mui-colors/blue #_(.-blue mui-colors)
                       :secondary mui-colors/orange #_(.-orange mui-colors)}
             :typography {:useNextVariants true}})))

(def pane (rf/subscribe [:pane]))

(def board-items (rf/subscribe [:board]))

(def signed-in-status (rf/subscribe [:signed-in-status]))

(defn authenticated-hook [signed-in-status]
  "Affect what is shown after logging in and out"
  (timbre/debug "Authenticated Status Changed:" signed-in-status)
  ; class supposed to be added by script in head of html file
  ; when returning from blockstack:
  (case signed-in-status
    true
    (js/document.documentElement.classList.add "authenticated")
    false
    (js/document.documentElement.classList.remove "authenticated")
    nil)
  ; preferably last as it is supposed to hide the landing
  (case signed-in-status
    (true false)
    (js/document.documentElement.classList.remove "reloading")
    nil))

(defn on-authenticated-changes []
  (authenticated-hook @signed-in-status))

(defstate authenticated-track
  :start (reagent/track! on-authenticated-changes)
  :end (reagent/dispose! authenticated-track))

(defn page [{:keys [open]}]
  (timbre/debug "Page:" open)
  [:div {:hidden (not open)}
    (case (or @pane :default)
      :profile [dev/user-profile-card {:user-data user-data}]
      :state [dev/state-inspector]
      :default [board-pane (or @board-items)])])

(defn app []
  [:<>
   [:> mui-ThemeProvider
    {:theme (custom-theme)}
    [:<>
       [:> mui/CssBaseline]
       [appbar]
       [page {:open (= @signed-in-status true)}]]]])
