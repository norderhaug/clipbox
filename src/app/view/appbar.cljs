(ns app.view.appbar
  (:require
   [taoensso.timbre :as timbre]
   ["@material-ui/core" :as mui]
   ["@material-ui/icons/FlashOn" :default LightningIcon]
   ["@material-ui/icons/AssignmentReturned" :default AssignmentReturned]
   ["@material-ui/icons/AccountCircle" :default AccountCircle]
   ["@material-ui/icons/FileCopy" :default CopyIcon]
   ["@material-ui/icons/MoreVert" :default MoreIcon]
   ["@material-ui/core/styles"
    :refer [makeStyles]]
   [re-frame.core :as rf]
   [reagent.core :as reagent]
   ;[app.clipboard :as clipboard]
   [app.transfer :as transfer]
   [app.lib.clipboard :as clipboard]
   [app.state :refer [app-state]]))

(def AppIcon AssignmentReturned)
(def PasteIcon AssignmentReturned)

(def debug (rf/subscribe [:debug]))

(defn lightning-button [{:keys [active]}]
  (let [action #(rf/dispatch [:user/request-funds active])]
    [:> mui/IconButton {:color "inherit"
                        :style {:color (if active "yellow" "inherit")}
                        :disabled true
                        :on-click action}
     [:> LightningIcon]]))

(def selected-sub (rf/subscribe [:selected]))

(defn copy-button [{:keys [active]}]
  (let [action #(clipboard/execute-copy)
        icon CopyIcon]
    (assert icon)
    [:> mui/IconButton {:color "inherit"
                        :disabled true #_(boolean (empty? @selected-sub))
                        :on-click action}
     [:> icon]]))


(defn paste-button [{:keys [active]}]
  (let [icon PasteIcon
        action #(clipboard/execute-paste)]
    (assert icon)
    [:> mui/IconButton {:color "inherit"
                        :on-click action}
     [:> icon]]))


(defn user-status-area [{:keys [signed-in-status]}]
  (let [state (reagent/atom {})
        open-menu #(swap! state assoc :anchor (.-currentTarget %))
        close-menu #(swap! state assoc :anchor nil)
        signout #(rf/dispatch [:app/exit])
        signin #(rf/dispatch [:app/signin])]
    (fn [{:keys [signed-in-status]}]
      (if (not @signed-in-status)
        [:div
         [:> mui/Button  {:color "inherit"
                          :on-click signin}
          [:> AccountCircle]
          (if (some? @signed-in-status)
            "Sign In")]]
        [:div
         [:> mui/IconButton {:aria-owns "menu-appbar"
                             :aria-haspopup true
                             :color "inherit"
                             :on-click open-menu}
          [:> MoreIcon]]
         [:> mui/Menu
          {:id "menu-appbar"
           :anchorEl (:anchor @state)
           :open (boolean (:anchor @state))
           :on-close close-menu}
          (for [{:keys [action label] :as item}
                [{:label "Sign Out" :action signout}
                 (if @debug {:label "-"})
                 (if @debug {:label "Inbox" :action  #(rf/dispatch [:pane nil])})
                 (if @debug {:label "Profile" :action #(rf/dispatch [:pane :profile])})
                 (if @debug {:label "State" :action  #(rf/dispatch [:pane :state])})]
                :when (some? item)]
            ^{:key label}
            [:> mui/MenuItem
             {:on-click #(do
                           (if action (action))
                           (close-menu))}
             label])]]))))


(def signed-in-status (rf/subscribe [:signed-in-status]))


(defn appbar []
  [:div {:style {:flex-grow 1}}
   [:> mui/AppBar {:position "static"}
    [:> mui/Toolbar {}
     [:> mui/Avatar
      {:style {:margin-right "1em"}}
      [:> AppIcon {:color :secondary}]]
     [:> mui/Typography {:variant "h6"
                         :style {:color "yellow"
                                 :flex 1}}
      "ClipBox"]
     #_
     (if @signed-in-status
       [:div {:style {:margin-right "1em"}}
        [copy-button]
        [paste-button]])
     #_
     [lightning-button {:active @signed-in-status}]
     [user-status-area {:signed-in-status signed-in-status}]]]])
