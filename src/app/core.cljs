(ns app.core
  (:require
   [reagent.core :as r]
   [mount.core :as mount]
   [taoensso.timbre :as timbre]
   [app.view.core :as view]
   [app.lib.blockstack :as blockstack33
    :refer [init-blockstack]]
   [app.transfer]
   [app.state]
   [app.events]
   [app.routing :as routing]
   [app.lightning]
   ; temporary
   [blockstack :as blockstack2
     :refer [UserSession AppConfig]]))

(defn mount-root []
  (r/render-component [view/app]
                      (.getElementById js/document "app")))

(defn ^:dev/before-load stop []
  (timbre/info "App stopping")
  (mount/stop)
  (timbre/info "App stopped"))

(defn ^:dev/after-load start []
;  (timbre/info "App init blockstack")
  (init-blockstack)
;  (timbre/info "App starting")
  (mount-root)
;  (timbre/info "App Mounting")
  (mount/start)
;  (timbre/info "App started")
  ;; routing should be last:
  (routing/init!))

(defn ^:export main
  []
  (start))
