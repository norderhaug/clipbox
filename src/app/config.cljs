(ns app.config)

(def data-storage ;; should be in settings
  {:path "v1x/data.edn"
   :options {:decrypt true}
   :reader cljs.reader/read-string
   :writer prn-str})
