(ns app.events
  (:require
   [taoensso.timbre :as timbre]
   [app.state :refer [app-state]]
   [re-frame.core :as rf]
   [mount.core :refer [defstate]]
   [app.config :as config]
   [app.lib.blockstack]
   [app.lightning :as lightning]))



(def initial-db
  {:debug false
   :data-storage config/data-storage
   :board [{:id "0"
            :title "Initial Clip"
            :type "text/plain"
            :data (str "Paste images, text and other content to share across devices.\n"
                       "Select clip to copy the content.\n")}]})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn log-event [& [label]]
  (rf/->interceptor
    :id      ::log-args
    :before  (fn [context]
               (let [event (get-in context [:coeffects :event])]
                 (timbre/debug (or label "Event:") event)
                 context))))

(rf/reg-event-db
 :initialize
 [(log-event)]
 (fn [db [_ initial]]
   (if (empty? db) initial db)))

(rf/reg-sub
 :db
 (fn [db [_ & query]]
   db))

(rf/reg-sub
 :debug
 (fn [{:keys [debug] :as db} [_ & query]]
   (if (some? debug) debug false)))

(rf/reg-event-db
 :debug
 [(log-event)]
 (fn [db [_ mode]]
   (assoc db :debug mode)))

(rf/reg-event-db
   :pane
   [(log-event)]
   (fn [db [_ mode]]
     (assoc db :pane mode)))

(rf/reg-sub
 :pane
 (fn [db query]
   (get db :pane)))

(rf/reg-event-db
   :count
   (fn [db [_ f]]
     (assoc db :count
            (f (get db :count 0)))))

(rf/reg-sub
 :count
 (fn [db query]
   (get db :count 0)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(rf/reg-event-db
 :file
 (fn [{:as db} [_ name content]]
   (assoc-in db [:file name] content)))

(rf/reg-sub
 :file
 (fn [db query]
   (-> (get db :file)
       (get query))))

(defn parse-json [s]
  (.parse js/JSON s))

(defn json-stringify [j]
  (.stringify js/JSON j))

(rf/reg-sub
 :board
 (fn [db [_ query]]
   (get db :board)))

(rf/reg-sub
 :selected
 (fn [db [_]]
   (filter :selected (:board db))))

(rf/reg-event-db
 :select
 (fn [{:as db} [_ item & [toggle?]]]
   (update db :board
           (fn [items]
             (map #(assoc % :selected
                          (and (= % item)
                               (or (not toggle?)
                                   (not (:selected %)))))
                  items)))))

(rf/reg-event-db
 :paste
 [(log-event)]
 (fn [{:as db} [_ {:as item}]]
   (update db :board conj item)))

(rf/reg-event-db
 :drag
 (fn [{:as db} [_ [status & args]]]
   (timbre/debug "Drag:" status args)
   (assoc db :drag status)))

(rf/reg-sub
 :drag
 (fn [db [_ query]]
   (get db :drag)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; APP ROUTING EVENTS

(rf/reg-event-db
 :app/home
 [(log-event)]
 (fn [{:as db} [_]]
   (timbre/info "Welcome!")
   db))

(rf/reg-event-fx
 :app/enter
 [(log-event)]
 (fn [{:keys [user-session data-storage] :as db}  [_ {:as item}]]
   (timbre/info "Enter App" (boolean user-session))))


(rf/reg-event-fx
 :app/exit
 (fn [{:as db} [_ {:as item}]]
   (timbre/info "Exit App")
   {:dispatch [:sign-user-out]}))

(rf/reg-event-fx
 :app/signin
 (fn [{:as db} [_ {:as item}]]
   (timbre/info "Signin")
   {:dispatch [:sign-user-in "/enter"]}))

; #################################
; PERSISTENT STATE

(rf/reg-event-db
 :state/loaded
 [(log-event)]
 (fn [{:as db} [_ {:as board}]]
   (timbre/debug "Loaded:" board)
   (if-not (empty? board)
     (assoc db :board board)
     db)))

(rf/reg-event-fx
 :state/load
 [(log-event)]
 (fn [{:keys [] {:keys [user-session data-storage]} :db :as fx} [_]]
   {:blockstack/store-file
    (assoc data-storage
           :user-session user-session
           :content (str (js/Date.now))
           :path "history.edn")
    :blockstack/load-file
    (assoc data-storage
           :user-session user-session
           :dispatch (fn [content]
                       (rf/dispatch [:state/loaded content])))}))

;#################################

(rf/dispatch-sync [:initialize initial-db])

#_
(rf/dispatch [:debug true])
