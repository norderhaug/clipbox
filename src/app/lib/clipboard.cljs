(ns app.lib.clipboard
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop]])
  (:require
   [cljs.core.async :as async
    :refer [<! chan close! alts! timeout put!]]
   [taoensso.timbre :as timbre]
   [app.lib.datatransfer :as datatransfer]
   [goog.fs.FileReader :default FileReader]))

"Generalized shim to provide clipboard functionality"

;; https://www.w3.org/TR/clipboard-apis/#async-clipboard-api
;; https://w3c.github.io/clipboard-apis/

; Legacy clipboard functions only in context of event handlers and cannot be
; used in async context... so careful if attempting to combine

; Chrome will soon allow copy/paste of images (6/30 2019! or now by enabling "Experimental Web Platform features")
; fixing a long standing bug: https://bugs.chromium.org/p/chromium/issues/detail?id=150835
; https://docs.google.com/document/d/1sMcajoYAQ_M_VOIn1RLokG1sVB7skjoeviqt9aZOcDk/edit

;; ## FIX: See the clipboard api and use callback for reactive functionality

(defn query-read-permission []
  (let [out (chan)]
    (-> (js/navigator.permissions.query #js{:name "clipboard-read"})
        (.then (fn [status] (timbre/info "Clipboard read status:" status)))
        (.then (fn [status] (put! out (.-state status))))
        (.catch #(timbre/warn %))
        (.finally #(close! out)))
    out))

(defn query-write-permission []
  (js/navigator.permissions.query #js{"name" "clipboard-write"}))

(defn modern-clipboard []
  ; see discussion at https://developers.google.com/web/updates/2018/03/clipboardapi
  (if (boolean js/navigator.clipboard)
    js/navigator.clipboard))

(defn insert-clip
  ([data {:keys [type] :as options}]
   (timbre/debug "Insert clip:" data options)
   (cond
     (not (modern-clipboard)) nil

     (and (= type "text/plain")
          js/navigator.clipboard.writeText)
     (let [clipboard js/navigator.clipboard]
       (-> (.writeText clipboard data)
           (.then (fn [response](timbre/debug "Copied to clipboard -" response))
                  (fn [err] (timbre/error "Failed to copy to clipboard" err)))))

     js/navigator.clipboard.write
     (let [clipboard js/navigator.clipboard
           js-options #js{:type type}
           ; item (new js/Blob [data] js-options)
           item (new js/ClipboardItem (clj->js {type data}))]
       (assert js/navigator.clipboard.write)
       (-> (.write clipboard [item])
           (.then (fn [response](timbre/debug "Copied to clipboard -" response))
                  (fn [err] (timbre/error "Failed to copy to clipboard" err))))
       true))))

(defn legacy-insert-clip [event data {:keys [type] :as options}]
  ; Important: Has to be in the context of the event, not async
  (timbre/debug "Legacy insert clip:" type data)
  (.setData (.. event -clipboardData) type data))


(defn blob-wrapper [handle]
  (fn [content]
    (cond
      (string?  content)
      (let [text content]
        (handle {:type "text/plain" :data text}))

      true
      (let [blob content]
        (timbre/debug "Blob" blob)
        (timbre/debug "FileReader:" goog.fs.FileReader)
        (timbre/warn "Blob:" blob)
        (let [fr goog.fs.FileReader]
          (-> blob
              (.then #(.readAsText fr %))
              (.then handle)
              (.catch #(timbre/error %))))))))

(defn load-clipboard-item [items handle]
  ; https://w3c.github.io/clipboard-apis/#clipboarditem
  ; Implementation based on Chrome 75.0.3770.80
  ; to handle read from clipboard without paste event
  ; reports as ClipboardItem but is really multiple
  ;; ## TODO: Factor FileReader uses to datatransfer
  (timbre/debug "Load clipboard item:"
                items (js-keys items)
                (aget items 0)(js-keys (aget items 0)))
  (doseq [ix (js-keys items)]
    (timbre/debug "IX" ix)
    (let [item (aget items ix)
          types (.-types item)]
      (timbre/debug "item types" types item)
      (if (empty? types)
        (-> (.getType item "text/plain")
            (.then (blob-wrapper #(handle [%]))))
        (doseq [type types]
          (let [blob (.getType item type)
                fr goog.fs.FileReader]
            (timbre/debug "DATA:" type blob fr (js-keys fr))
            (-> blob
                (.then #(.readAsText fr %))
                (.then (blob-wrapper #(handle [%]))))))))))


(defn copy-noop [& _]
  (timbre/warn "No copy handler"))


(def copy-handler_ (atom copy-noop))


(defn copy-event-handler [event]
  (.preventDefault event) ;; if not just use default like selected text
  (@copy-handler_
    (fn [items]
      (timbre/debug "Copy event handler callback:" items)
      (let [[content] items] ;; ## TODO: handle rest of items
        (when content
          (or
           (insert-clip
            (:data content) {:type (:type content)})
           (legacy-insert-clip
            event (:data content) {:type (:type content)})))))))


(defn paste-noop [& _]
  (timbre/warn "No paste handler"))


(def paste-handler_ (atom paste-noop))


(defn paste-handler [data]
  ;; TODO: Verify data
  (@paste-handler_ data))


(defn paste-event-handler [event]
  (timbre/info "Paste from clipboard" event)
  (if-let [clip-data (.-clipboardData event)]
    (do
      (timbre/info "Load paste datatransfer")
      (datatransfer/load-datatransfer clip-data paste-handler)
      (.preventDefault event))
    (let [text (.. js/navigator -clipboard readText)]
      (timbre/info "Text=" text)
      (.preventDefault event)
      (-> text
          (.then (fn [text]
                   (timbre/info "Pasted:" text)
                   (paste-handler [{:type "text/plain"
                                    :data text}])))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; External

(defn subscribe! [{:keys [copy paste]}]
  "Subscribe to paste events (with callback)"
  (timbre/info "Enable clipboard events")
  (let [root (.getElementById js/document "app")]  ;;  ## Should just be on js/document?
    (when paste
      (.addEventListener root goog.events.EventType.PASTE
                         paste-event-handler)
      (.addEventListener js/document goog.events.EventType.COPY
                         copy-event-handler)
      (reset! paste-handler_ paste)
      (reset! copy-handler_ copy))))


(defn unsubscribe! []
  (timbre/info "Disable clipboard events")
  (let [root (.getElementById js/document "app")] ;;  ## Should just be on js/document?
    (.removeEventListener root goog.events.EventType.PASTE
                          paste-event-handler)
    (.removeEventListener js/document goog.events.EventType.COPY
                          copy-event-handler)
    (reset! paste-handler_ paste-noop)
    (reset! copy-handler_ copy-noop)))


(defn execute-paste []
  "Manually execute paste from code"
  (timbre/debug "Execute Paste")
  (if-let [clipboard (modern-clipboard)]
    (go-loop [access (<! (query-read-permission))]
      (cond
        (boolean clipboard.read)
        (-> (.read clipboard)
            (.then #(load-clipboard-item % paste-handler)))
        (boolean clipboard.readText)
        (-> (.readText clipboard)
            (.then #(paste-handler
                     [{:type "text/plain"
                       :data %}])))
        :else
        (do
          (timbre/warn "Clipboard access is not enabled by the browser" (js-keys clipboard))
          (.execCommand js/document "paste"))))
    ; legacy, may not work
    ; ## Consider also keeping local copy for internal paste without system clipboard
    (do
      (timbre/debug  "Fallback on executing legacy paste")
      (.execCommand js/document "paste"))))


(defn execute-copy []
  (timbre/debug "Execute Copy")
  (.execCommand js/document "copy"))
