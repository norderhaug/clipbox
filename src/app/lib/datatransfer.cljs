(ns app.lib.datatransfer
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop]])
  (:require
   [cljs.core.async :as async
    :refer [<! chan close! alts! timeout put!]]
   [goog.fx.DragDrop]
   [goog.fx.DragDropGroup]
   [goog.fs :as fs]
   [goog.fs.FileReader :default FileReader]
   [taoensso.timbre :as timbre]))

;; Common interface for data from paste and drop events
;; Canonical in ClipBox

(defn as-url [object]
  {:pre [(some? object)]
   :post [string?]}
  (if (goog.fs.url.browserSupportsObjectUrls)
    (goog.fs.url.createObjectUrl object)
    (timbre/error "Browser doesn't support object URLs")))


(defn get-as-string [item]
  {:pre [(some? item)]}
  ; https://developer.mozilla.org/en-US/docs/Web/API/DataTransferItem/getAsString
  ; Mozilla specify callback but others say it returns a string...
  ; so try to cover both, or find a goog for itt
  (let [out (async/promise-chan)
        result (.getAsString item (fn [value] (put! out value)))]
    (when result
      (put! out result))
    out))


(defn file-as-url [file]
  {:pre [(some? file)]}
  (timbre/debug "File as URL (async):" file)
  ; https://google.github.io/closure-library/api/goog.fs.FileReader.html
  (let [out (async/promise-chan)]
    (-> (.readAsDataUrl goog.fs.FileReader file)
        (.then (fn [value] (put! out value)))
      #_
      (let [reader (new js/FileReader)]
        (set! (.-onload reader)
              (fn [data]
                (put! out data)))
        (.readAsDataUrl reader file)))
   out))


(defn file-as-blob-async [file cb]
  (let [reader (new js/FileReader)]
    (set! (.-onload reader)
          #(cb (.-result reader)))
    (.readAsArrayBuffer reader file)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; 1. When dragging text string from other app and dropping in Chrome 75.0.3770.80,
;;    the datatransfer contains a list of a single DataTransferItem of type "text/plain".
;;    (.getData transfer "text/plain") returns the text as a string.
;;    The filelist will be empty.

(defn- transfer-item-promise [transfer-item {get-data :get-data}]
  ; https://developer.mozilla.org/en-US/docs/Web/API/DataTransferItem
  ; Return a promise-chan with the loaded transfer item.
  ; To counter legacy concerns with the extent of the transfer-item,
  ; do NOT execute async before it is required, and
  ; definitely do NOT enclose in a go form.
  (timbre/debug "Load datatransfer item:" transfer-item)
  (let [out (async/promise-chan)
        handle #(put! out (or % {}))]
    (if (string? transfer-item)
      (handle {:type  "text/plain" :data transfer-item})
      (let [item transfer-item ;; type DataTransferItem
            item-kind (.-kind item)
            item-type (.-type item)]
        (timbre/info "Clip=" (.stringify js/JSON item)
                     (js->clj item) item-kind item-type (js-keys item))
        (cond
          (and (= item-kind "file")
               ;; weird but sometimes false
               (some? (.getAsFile item)))
          (let [file (.getAsFile item) ;; don't do in async as it may no longer exist
                url (as-url file)]
            (timbre/debug "Handle file datatransfer" handle)
            (handle         {:kind item-kind
                             :type item-type
                             :data file
                             :url  url}))

          (or (= item-type "text/plain")
              (= item-type "string"))
          (let [f #(handle {:type (or item-type "text/plain")
                            :data %})]
            (if true
              (f (get-data item-type))
              (do
                (timbre/debug "Async get string")
                (.getAsString item f))))

          (= item-type "text/html")
          (let [f #(handle {:type item-type
                            :data %})]
            (.getAsString item f))

          (= item-type "image/png")
          (let [clip item]
            (if-not (empty? clip) ;; empty string when no value per spec
              (let [_ (timbre/debug "->" clip (type clip)(some? clip))
                    url nil]
                (handle {:type item-type :data clip :url url}))
              (do
                (timbre/warn "No clip content for type:" item-type)
                (handle nil))))

          true ;; images etc
          (do (timbre/warn "Type not supported:" item-type)
            (handle nil)))))
    out))

(defn- sync-load [transfer]
  ; Returns an async channel with a vector of transfer items
  ; hairy to work around browser inconsistency in having async api yet extent limited to event
  ; delay async block to as late as possible, avoiding accessing transfer items outside their extent
  ; goal is to have resolved as much of the data as possible by time of return
  ; do NOT get tempted to spin off async processes here...
  (let [get-data #(.getData transfer %)]
    (->> (js/Array.from (.-items transfer))
         (js->clj)
         (map #(transfer-item-promise % {:get-data get-data}))
         (doall)
         (async/map vector))))

(defn load-datatransfer [transfer handle]
  ; https://developer.mozilla.org/en-US/docs/Web/API/DataTransferItem
  ;; The content in the transfer may only exists for the duration of the call, so no cljs.core.async.macros
  ;; Yet some of the extracting functions are async...
  (timbre/debug "Load datatransfer:" transfer)
  (let [types (map js->clj (.-types transfer))
        items (.-items transfer)
        input (if true
                (js->clj (js/Array.from items))
                (map #(.getData transfer %) types))
        finalize (fn [ch]
                    (timbre/debug "Ready to finalize transfer")
                    (go-loop [result (<! ch)]
                      (timbre/debug "Finalizing transfer" result)
                      (handle result)))]
    (cond
      ; has to be synchronous, in scope of transfer
      (and
       (= 1 (.-length items))
       (= "text/plain" (.-type (aget items 0))))
      (handle [{:type "text/plain"
                :data (.getData transfer "text/plain")}])
      :else (-> (sync-load transfer) ; do NOT enclose this in a go!
                (finalize)))))

(defn file->cljs [file]
  (timbre/debug "Decode file (async)" file)
  ; keep as much as possible outside the go for legacy web api compatibility
  (let [url (file-as-url file)
        result
        {:kind "file"
         :name (.-name file)
         :type (.-type file)
         :size (.-size file)
         :modified (.-lastModified file)
         :data file}]
    (go (assoc result :url (<! url)))))

(defn dnd-drop-wrapper [drop-handler]
  ; wrapper returns a drop handler for dnd/subscribe!
  ; calling drop-handler with cljs values
  (fn [e files]
    (timbre/debug "Drop:" e files (.-length files))
    (let [files (js->clj (js/Array.from files))]
      (cond
        (not (empty? files))
        (let [files (doall (map file->cljs files))]
          (timbre/debug "Files dropped:" files)
          (go
           (->> (<! (async/map vector files))
                (map vector)
                (apply drop-handler))))

        :else
        (let [data-transfer (.-dataTransfer e)]
          (timbre/debug "Data Transfer:" data-transfer)
          (load-datatransfer data-transfer
                             #(do
                                (timbre/debug "===============>" %)
                                (drop-handler %))))))))
