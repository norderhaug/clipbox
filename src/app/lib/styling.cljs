(ns app.lib.styling
  (:require
   [taoensso.timbre :as timbre]
   [reagent.core :as reagent]
   ["react" :as react] ;; needed by styles
   ["@material-ui/core/styles" :as styles
    :refer [makeStyles withTheme]]))

;; ##FIX: canonical in person8 and should be moved back with tjhe changes. Aug 2019.

;; Reagent integration of react styling hooks (in-progress)
;; Alternative names: styles; theme;

(defn with-theme [f]
  (-> (withTheme (fn [theme]
                   (reagent/as-element
                     (f theme))))
      (reagent/adapt-react-class)))

(defn mui-styles [f]
 {:post [fn?]}
 "Define a react hook with custom styles for mui on toplevel that are injected into the css of the page.
  Used to generate a style that is based on the current theme.
  Returns a function that when evaluated provides a map to mui classnames for the styles."
 (let [styles (styles/makeStyles
               (fn [theme]
                 (clj->js (f theme))))]
    (fn []
      (js->clj (styles) :keywordize-keys true))))

#_ ;; example
(def use-styles (mui-styles (fn [theme]
                              {:grid-bg {:padding (.spacing theme 1)}})))

(defn Block [props extra]
  {:pre [(some? props)]}
  "React function component using mui-styles"
  ;; Can't use Ratoms here!
  ;; Can take a function as children, in which case it is called with class
  (let [use-styles (aget props "muiStyles") ; hook; aget as property name should not be minified
        ensure-seq #(if (coll? %) % (vector %))
        class-list (map keyword
                        (-> (.-className props)
                            (js->clj)
                            (ensure-seq)))
        classes (if (fn? use-styles)
                  (use-styles)
                  (timbre/warn "Block with no styles"))
        names (->> (map #(get classes %) class-list)
                   (clojure.string/join " "))
        children (.-children props)
        thunk (if (fn? children) children)]
    (reagent/as-element
       [:div {:class-name names}
        (if thunk
          (thunk classes)
          children)])))


(def block (reagent/adapt-react-class Block))

#_
(def page
  (let [component (reagent/as-element page_)]
    (fn [props]
      (->
       (withTheme (fn [theme]
                    (timbre/debug "with theme" (js->clj theme))
                    component))
       #_(reagent/adapt-react-class)
       (vector props)))))
