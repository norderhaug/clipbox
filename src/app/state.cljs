(ns app.state
  (:require
   [reagent.core :refer [atom]]
   [mount.core :as mount
     :refer [defstate]]
   [re-frame.core :as rf]
   [taoensso.timbre :as timbre]
   [reagent.core :as reagent]
   [app.lib.blockstack]))

(defonce app-state (atom {:count 0}))

(def signed-in-status (rf/subscribe [:signed-in-status]))

(defstate observing-signin
  :start (reagent/track!
          #(when (= @signed-in-status true)
             (rf/dispatch [:state/load])))
  :end (reagent/dispose! observing-signin))
