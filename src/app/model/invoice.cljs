(ns app.model.invoice)

(defrecord Invoice [encoded decoded])

(defn create [encoded decoded]
  (->Invoice encoded decoded))

#_
(->Invoice nil nil)
