(ns app.model.clip
  (:require
   [clojure.spec.alpha :as s]))

(s/def ::mime-image #(clojure.string/starts-with? % "image/"))

(s/def ::mime-text-plain #(= % "text/plain"))

(s/def ::mime-text-html #(= % "text/html"))

(s/def ::variation (s/or :map (s/keys :req-un [type] :opt-un [])))

; A sequence of one or more representations of the same content

(defrecord Clip [variations])
