(ns app.lightning
  (:require
   ["bolt11" :as bolt11]))

(def invoice-example
  "lntb420n1pw0ny2ppp5u6vpdx5y0rrwhr2rq7estrxgfj8l389l89zxa4xgashcyjmu8ulqdqd2pshjgzp2dq4qcqzpg2mvaedzd64l2klz20gm0nd9v4jzyk43taj8pstl9spu7557w3pfqgu82fw60adyruk97w2xxstzze4yge6cnx536umy27fwewjwr3vqq6r2yrw")

(defn decode-invoice [s]
  (js->clj (bolt11/decode s) :keywordize-keys true))

(defn valid-invoice? [s]
  {:pre [string? s]}
  (and (= "ln" (clojure.string/lower-case (subs s 0 2)))
       ;; wasteful - TODO: optimize
       (try (boolean (bolt11/decode s))
         (catch :default e false))))

#_
(decode-invoice invoice-example)

#_
(list bolt11/decode)

#_
(decode-invoice "xxxyyyzzz")

#_
(valid-invoice invoice-example)
