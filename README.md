# ClipBox

[![Netlify Status](https://api.netlify.com/api/v1/badges/58ad42b4-e36f-4be5-8693-98b01bafa70d/deploy-status)](https://app.netlify.com/sites/clipbox/deploys)

Netlify build for cljs: https://github.com/thheller/netlify-cljs

## Development Workflow

    npm install

Then in Atom: `Packages:proto-repl:Start REPL` => option-cmd-L


### Troubleshooting
first do some or all of these:

    npm run clean
    lein do clean, deps, compile
    npm run dev

    if using proto-repl, first stop server with ctrl-c

## Setup And Run

#### Install dependencies
```shell
yarn install || npm install
```

#### Transpile JSX with Babel

```shell
npm run start
```

    "babel-plugin-css-modules-transform": "^1.6.2",
    "babel-plugin-transform-import-css": "0.1.6",
    "babel-plugin-transform-react-jsx": "^6.24.1",


    "babel-plugin-react-css-modules": "3.4.2",

    "babel-cli": "^6.20.0",

"transform-react-jsx",
["transform-import-css", {
"generateScopedName": "lib-[folder]-[name]-[local]-[hash:base64:4]"
}],
"react-css-modules",

 -d ./src/gen/ --copy-files

#### Run dev server
```shell
yarn dev || npm run dev || lein run -m shadow.cljs.devtools.cli --npm watch app
```

(require '[shadow.cljs.devtools.server :as server])
(require '[shadow.cljs.devtools.api :as shadow])
(server/start!)

(shadow/watch :app)
(shadow/nrepl-select :app)


#### Compile an optimized version

```shell
yarn release || npm run release
```
