(defproject clipbox "0.1.0-SNAPSHOT"
  :description "A leiningen project to use proto repl with shadow-cljs."
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/core.async "0.4.490"]
                 [thheller/shadow-cljs "2.8.42"]
                 ; [binaryage/devtools "0.9.10"]
                 [proto-repl-charts "0.3.1"]
                 [proto-repl "0.3.1"]
                 [com.taoensso/timbre "4.10.0"]
                 [reagent "0.9.0-rc1"]
                 [re-frame "0.10.6"]
                 [metosin/reitit "0.3.9"]
                 [mount "0.1.16"]
                 [cljs-http "0.1.46"]
                 [cljs-drag-n-drop "0.1.0"]
                 [person8org/person8 "7361aa0e9a526655d711bf1d1b6e5d9609574dda"]
                 #_
                 [person8org/person8 "91a2850272edfab44a83cba83ae2edad343d2029"]
                 #_
                 [njordhov/person8 "9820833460a144914dcb71318a8972c2d4fe216c"]
                 #_
                 [njordhov/person8 "a4532f0b636e18ffee956054d0edf67205cee897"]]

  :min-lein-version "2.8.1"

  :plugins [[reifyhealth/lein-git-down "0.3.5"]]

  :repositories [["public-github" {:url "git://github.com"}]
                 ["public-bitbucket" {:url "git://bitbucket.org"}]]

  :source-paths ["src"]

  :repl-options {:nrepl-middleware
                 [#_shadow.cljs.devtools.server.nrepl/cljs-load-file
                  #_shadow.cljs.devtools.server.nrepl/cljs-eval
                  shadow.cljs.devtools.server.nrepl/cljs-select
                  #_cemerick.piggieback/wrap-cljs-repl]}

  :profiles
  {:dev {:source-paths ["dev" "src"]}})
